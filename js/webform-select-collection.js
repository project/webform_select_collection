/**
 * @file
 * Webform select collection behaviors.
 */

(function ($, Drupal) {
  /**
   * Behavior description.
   */
  Drupal.behaviors.webformSelectCollection = {
    attach(context, settings) {
      $(once('table-select', 'th.select-all-collection', context))
        .closest('table')
        .each(Drupal.tableSelectCollection);
    },
  };

  Drupal.tableSelectCollection = function () {
    if (
      $(this).find('td.webform-select-collection-select select').length === 0
    ) {
      return;
    }

    const $table = $(this);
    const $selectBoxes = $table.find(
      'td.webform-select-collection-select select:enabled',
    );
    const selectAllTitle = Drupal.t('Select value for all rows in this table');
    const $selectAll = $selectBoxes.first().parents('.form-item').clone();

    $selectAll.find('select').attr('title', selectAllTitle);
    // Select all element should never be required.
    $selectAll
      .find('select')
      .removeAttr('required')
      .removeAttr('aria-required')
      .removeClass('required');
    $table.find('th.select-all-collection').prepend($selectAll);

    const resetSelectAll = function resetSelectAll() {
      const $select = $table.find('th.select-all-collection select');
      const defaultValue = $select.find('option').first().attr('value');

      $select.val(defaultValue);
    };

    $selectAll.on('change', function (event) {
      const targetValue = $(event.target).val();

      $selectBoxes.each(function () {
        const $selectBox = $(this);
        const selectValue = $selectBox.val();

        const stateChanged = selectValue !== targetValue;

        if (stateChanged) {
          $selectBox.val(targetValue).trigger('change');
        }
      });
    });

    $selectBoxes.on('change', function () {
      if ($selectAll.find('select').val() !== $(this).val()) {
        resetSelectAll();
      }
    });
  };
})(jQuery, Drupal);
